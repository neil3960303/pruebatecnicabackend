# Guía de Instalación para la Aplicación Backend en Nest.js
----
## Levantar la Base de Datos con Docker:
**IMPORTANTE:**
**Elimina** la carpeta `postgres_data` (no lo puse en el archivo git ignore, pero solo lo elimina antes de todo y no habrá problema) este directorio es la base de datos que genera docker, nada más, por eso se debe eliminar para que no se choque al instalar en su computadora

Ejecuta el siguiente comando para levantar la base de datos PostgreSQL usando Docker Compose (asegúrate de tener Docker instalado y corriendo):

```bash
docker-compose up
````

Esto iniciará tu base de datos PostgreSQL según la configuración en tu archivo `docker-compose.yml`.

### Instalamos los paquetes y dependencias necesarias:
```bash
npm i
````
### Realizamos el build
```bash
npm run build
````
### Aplicar Migraciones:
Después de que la base de datos esté en funcionamiento, ejecuta el siguiente comando para aplicar las migraciones:
```bash
npm run migration:run
````
Este comando ejecutará las migraciones utilizando TypeORM.
### Ejecutar Seeders:
Una vez que las migraciones se hayan aplicado con éxito, ejecuta el siguiente comando para ejecutar los seeders:
```bash
npm run seed
````
Esto poblacionará la base de datos con datos de prueba si es necesario.
### Iniciar la Aplicación:
Finalmente, puedes iniciar la aplicación Nest.js con el siguiente comando:
```bash
npm start
````
La aplicación se ejecutara en `localhost:4000/noticias/`
### Recomendaciones:
- Tener instalado Node JS
- Verifica que todas las dependencias estén instaladas utilizando `npm install` o `npm i` antes de ejecutar los comandos.
- Asegúrate de tener Docker instalado y en funcionamiento antes de levantar la base de datos.