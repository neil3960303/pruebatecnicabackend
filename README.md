# Backend - Prueba Técnica

##### **Nombre del Postulante:** Neil Ángel Graneros Flores
----
## Descripción del Proyecto:

Este proyecto aborda las consignas proporcionadas utilizando las siguientes tecnologías:

- Node.js 21.5
- Nest.js 10.3.0
- TypeORM 0.3.19
- PostgreSQL 16
- Docker

El servidor desempeña las funciones de CRUD para una entidad llamada "noticia". Se implementó Docker para montar la base de datos PostgreSQL, y además se crearon migraciones y seeders para crear tablas y almacenar datos en la base de datos. El servidor está desarrollado en Nest.js y TypeScript.

----
### Mi opinión al realizar el proyecto
En cuanto a mi opinión sobre la práctica, la considero un reto valioso, ya que nunca había trabajado con estas tecnologías. Fue enriquecedor, ya que pude comprender los fundamentos de cada tecnología, y de hecho, seguiré aprendiendo. Agradezco la oportunidad. ¡Buen día!

----
### Contacto:
correo: neilgraneros11@gmail.com    
cel: 60160883