import { Noticias } from '../src/noticias/noticias.entity';
import { DataSource, DataSourceOptions } from 'typeorm';
const path = require('path');
export const dataSourceOptions: DataSourceOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'root',
  password: '12345',
  database: 'noticiasdb',
  //synchronize: false,//en desarrollo
  synchronize: true,
  logging: true,
  //entities: [Noticias], //en desarrollo
  entities: ['dist/**/*.entity{.ts,.js}'],
  //migrations: ['db/migrations/*.ts'], //en desarrollo
  migrations:['dist/db/migrations/*.js'],
  migrationsTableName: 'migrations_meta',
};
const dataSource = new DataSource(dataSourceOptions);
export default dataSource;
