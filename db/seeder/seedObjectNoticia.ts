export const noticaSeed = [
  {
    titulo: 'Descubren nuevas especies en la selva amazónica boliviana',
    fechaPubli: '2024-01-01',
    lugar: 'Amazonía Boliviana',
    autor: 'Biólogo Boliviano',
    contenido:
      'Investigadores han identificado varias especies nuevas de flora y fauna en la selva amazónica de Bolivia.',
  },
  {
    titulo: 'Inauguración del nuevo centro cultural en La Paz',
    fechaPubli: '2024-01-02',
    lugar: 'La Paz, Bolivia',
    autor: 'Ministra de Cultura',
    contenido:
      'Se celebra la apertura del moderno centro cultural, destinado a promover el arte y la cultura boliviana.',
  },
  {
    titulo: 'Bolivia lidera proyecto de energía renovable en América Latina',
    fechaPubli: '2024-01-03',
    lugar: 'Cochabamba, Bolivia',
    autor: 'Ingeniero Boliviano',
    contenido:
      'El país sudamericano está a la vanguardia en la implementación de tecnologías sostenibles para la generación de energía.',
  },
  {
    titulo: 'Celebración del Festival de la Diablada en Oruro',
    fechaPubli: '2024-01-04',
    lugar: 'Oruro, Bolivia',
    autor: 'Folklorista Boliviano',
    contenido:
      'La colorida y tradicional festividad de la Diablada atrae a visitantes de todo el mundo a la ciudad de Oruro.',
  },
  {
    titulo: 'Bolivia avanza en la investigación espacial',
    fechaPubli: '2024-01-05',
    lugar: 'Centro Espacial Boliviano',
    autor: 'Científico Espacial Boliviano',
    contenido:
      'El país continúa su desarrollo en la exploración del espacio con nuevos logros en la investigación aeroespacial.',
  },
  {
    titulo: 'Desfile patrio en Sucre conmemora la independencia de Bolivia',
    fechaPubli: '2024-01-06',
    lugar: 'Sucre, Bolivia',
    autor: 'Historiador Boliviano',
    contenido:
      'La ciudad de Sucre se viste de gala para celebrar el aniversario de la independencia de Bolivia con un gran desfile patrio.',
  },
  {
    titulo: 'Inauguración de la nueva línea ferroviaria interdepartamental',
    fechaPubli: '2024-01-07',
    lugar: 'Santa Cruz - La Paz, Bolivia',
    autor: 'Ingeniero de Transporte Boliviano',
    contenido:
      'Se pone en funcionamiento la moderna línea de tren que conectará eficientemente las dos principales ciudades bolivianas.',
  },
  {
    titulo: 'Bolivia destaca en la producción de alimentos orgánicos',
    fechaPubli: '2024-01-08',
    lugar: 'Valles de Bolivia',
    autor: 'Agricultor Boliviano',
    contenido:
      'Los agricultores bolivianos logran reconocimiento internacional por su compromiso con la producción de alimentos orgánicos y sostenibles.',
  },
  {
    titulo: 'Bolivia acoge el Congreso Internacional de Medicina Tradicional',
    fechaPubli: '2024-01-09',
    lugar: 'La Paz, Bolivia',
    autor: 'Médico Tradicional Boliviano',
    contenido:
      'Expertos de todo el mundo se reúnen en Bolivia para intercambiar conocimientos sobre medicina tradicional y prácticas ancestrales.',
  },
  {
    titulo: 'Bolivia lidera esfuerzos en la conservación del cóndor andino',
    fechaPubli: '2024-01-10',
    lugar: 'Reserva Natural de Apolobamba',
    autor: 'Ecologista Boliviano',
    contenido:
      'La reserva natural boliviana se convierte en un bastión clave para la protección y conservación del majestuoso cóndor andino.',
  },
  {
    titulo:
      'Nueva iniciativa para promover el turismo sostenible en el Salar de Uyuni',
    fechaPubli: '2024-01-11',
    lugar: 'Salar de Uyuni, Bolivia',
    autor: 'Operador Turístico Boliviano',
    contenido:
      'Se lanzan proyectos para fomentar el turismo sostenible y preservar la belleza única del Salar de Uyuni.',
  },
  {
    titulo: 'Bolivia destaca en la producción de cine indígena',
    fechaPubli: '2024-01-12',
    lugar: 'Cochabamba, Bolivia',
    autor: 'Cineasta Indígena Boliviano',
    contenido:
      'La cinematografía indígena boliviana gana reconocimiento internacional por su contribución única al mundo del cine.',
  },
  {
    titulo: 'Bolivia implementa programa de educación digital en todo el país',
    fechaPubli: '2024-01-13',
    lugar: 'Escuelas Bolivianas',
    autor: 'Ministra de Educación',
    contenido:
      'El país avanza en la implementación de programas educativos digitales para mejorar el acceso y la calidad de la educación.',
  },
  {
    titulo: 'Bolivia organiza feria internacional de artesanía en La Paz',
    fechaPubli: '2024-01-14',
    lugar: 'La Paz, Bolivia',
    autor: 'Artista Boliviano',
    contenido:
      'La feria destaca la rica tradición artesanal boliviana, atrayendo a visitantes y comerciantes de todo el mundo.',
  },
  {
    titulo: 'Bolivia anuncia proyecto de energía solar a gran escala',
    fechaPubli: '2024-01-15',
    lugar: 'Región Solar de Bolivia',
    autor: 'Ingeniero en Energías Renovables',
    contenido:
      'El país se embarca en un ambicioso proyecto para aprovechar al máximo la energía solar y promover la sostenibilidad.',
  },
];
