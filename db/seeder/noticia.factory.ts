import { Faker } from '@faker-js/faker';
import { Noticias } from '../../src/noticias/noticias.entity';
import { setSeederFactory } from 'typeorm-extension';

export const NoticiasFactory = setSeederFactory(Noticias, (faker: Faker) => {
  const noticia = new Noticias();
  noticia.titulo = faker.company.name();
  noticia.autor = faker.person.fullName();
  noticia.fechaPubli = faker.date.past();
  noticia.lugar = faker.location.city();
  noticia.contenido = faker.lorem.paragraph();
  return noticia;
});