import { Noticias } from '../../src/noticias/noticias.entity'
import { DataSource, DataSourceOptions } from 'typeorm';
import { SeederOptions, runSeeders } from 'typeorm-extension';
import { NoticiasFactory } from './noticia.factory';
import { MainSeeder } from './main.seeder';

export const dataSourceOptions: DataSourceOptions & SeederOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'root',
  password: '12345',
  database: 'noticiasdb',
  // entities: [__dirname + '/**/*.entity{.ts,.js}'],
  //migrations: ['db/migrations/*{.ts,.js}'],
  synchronize: false,
  logging: true,
  //entities: ['src/**/*.entity.ts'],
  entities: [Noticias],
  //entities: [__dirname +'../src/**/*.entity{.ts,.js}'],
  factories: [NoticiasFactory],
  seeds: [MainSeeder],
};
const dataSource = new DataSource(dataSourceOptions);

dataSource.initialize().then(async () => {
  await dataSource.synchronize(true);
  await runSeeders(dataSource);
  process.exit();
});

export default dataSource;
