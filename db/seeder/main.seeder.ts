import { Noticias } from '../../src/noticias/noticias.entity';
import { DataSource, Repository } from 'typeorm';
import { Seeder, SeederFactoryManager } from 'typeorm-extension';
import { noticaSeed } from './seedObjectNoticia';

export class MainSeeder implements Seeder {
  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager,
  ): Promise<any> {
    const noticiasRepository = dataSource.getRepository(Noticias);

    // Crea noticias manuales
    await this.createManualNoticias(noticiasRepository);
    //
    const noticiaFactory = factoryManager.get(Noticias);
    await noticiaFactory.saveMany(7);
  }

  private async createManualNoticias(noticiasRepository: Repository<Noticias>): Promise<void> {
    const noticiasToCreate = noticaSeed.map((noticia) =>
      noticiasRepository.create(noticia),
    );
    await noticiasRepository.save(noticiasToCreate);
  }
}
