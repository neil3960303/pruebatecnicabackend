import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'noticias' })
export class Noticias {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  titulo: string;

  @Column({ type: 'date', default: () => 'CURRENT_TIMESTAMP' })
  fechaPubli: Date;

  @Column()
  lugar: string;

  @Column()
  autor: string;

  @Column()
  contenido: string;
}
