import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { NoticiasService } from './noticias.service';
import { Noticias } from './noticias.entity';
import { CreateNoticiaDTO, UpdateNoticiaDTO } from './dto/noticias.dto';

@Controller('noticias')
export class NoticiasController {
  constructor(private noticiasService: NoticiasService) {}

  @Get()
  getNoticias(
    @Query('q') query: string,
    @Query('page') page: number,
    @Query('limit') limit: number,
  ) {
    return this.noticiasService.getNoticias(query, page, limit);
  }

  @Get('/fecha')
  getOrdenarNoticias(@Query('page') page: number, @Query('limit') limit: number) {
    return this.noticiasService.getNoticiasFecha(page, limit);
  }

  @Get('pagination')
  async find(@Query('page') page: number, @Query('limit') limit: number) {
    return this.noticiasService.getPaginacion(page, limit);
  }

  @Get(':id')
  getNoticia(@Param('id', ParseIntPipe) id: number): Promise<Noticias> {
    return this.noticiasService.getNoticia(id);
  }

  @Post()
  createNoticias(@Body() newNoticia: CreateNoticiaDTO) {
    return this.noticiasService.createNoticia(newNoticia);
  }

  @Patch(':id')
  updateNoticia(
    @Param('id', ParseIntPipe) id: number,
    @Body() noticia: UpdateNoticiaDTO,
  ) {
    return this.noticiasService.updateNoticia(id, noticia);
  }

  @Delete(':id')
  deleteNoticia(@Param('id', ParseIntPipe) id: number) {
    return this.noticiasService.deleteNoticia(id);
  }
}
