import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Noticias } from './noticias.entity';
import { Like, Repository } from 'typeorm';
import { CreateNoticiaDTO, UpdateNoticiaDTO } from './dto/noticias.dto';

@Injectable()
export class NoticiasService {
  constructor(
    @InjectRepository(Noticias)
    private noticiasRepository: Repository<Noticias>,
  ) {}

  async createNoticia(noticia: CreateNoticiaDTO) {
    const noticiaFound = await this.noticiasRepository.findOne({
      where: { titulo: noticia.titulo },
    });
    if (noticiaFound) {
      throw new HttpException('La noticia ya existe', HttpStatus.CONFLICT);
    }

    const newNoticia = this.noticiasRepository.create(noticia);
    return this.noticiasRepository.save(newNoticia);
  }
  async getNoticias(
    query?: string,
    page: number = 1,
    limit: number = 10,
  ): Promise<{ data: Noticias[]; total: number }> {
    const resultado: any = query
      ? {
          titulo: Like(`%${query}%`),
        }
      : {};
    const [data, total] = await this.noticiasRepository.findAndCount({
      where: resultado,
      take: limit,
      skip: (page - 1) * limit,
      order: { id: 'ASC' },
    });

    return { data, total };
  }
  getNoticiasQuery(query?: string) {
    const resultado = query
      ? {
          titulo: Like(`%${query}%`),
        }
      : {};
    return this.noticiasRepository.find({ where: resultado });
  }

  async getNoticiasFecha(
    page: number = 1,
    limit: number = 10,
  ): Promise<{ data: Noticias[]; total: number }> {
    const [data, total] = await this.noticiasRepository.findAndCount({
      take: limit,
      skip: (page - 1) * limit,
      order: {
        fechaPubli: 'DESC',
      },
    });
    return { data, total };
  }

  async getPaginacion(
    page: number = 1,
    limit: number = 10,
  ): Promise<{ data: Noticias[]; total: number }> {
    const [data, total] = await this.noticiasRepository.findAndCount({
      take: limit,
      skip: (page - 1) * limit,
    });

    return { data, total };
  }
  async getNoticia(id: number) {
    const noticiaFound = await this.noticiasRepository.findOne({
      where: { id },
    });
    if (!noticiaFound) {
      throw new HttpException('Noticia no encontrada', HttpStatus.NOT_FOUND);
    }
    return noticiaFound;
  }
  async updateNoticia(id: number, noticia: UpdateNoticiaDTO) {
    const noticiaFound = await this.noticiasRepository.findOne({
      where: { id },
    });

    if (!noticiaFound) {
      throw new HttpException('Noticia no encontrada', HttpStatus.NOT_FOUND);
    }

    const updateNoticia = Object.assign(noticiaFound, noticia);
    return this.noticiasRepository.save(updateNoticia);
  }
  async deleteNoticia(id: number) {
    const noticiaDeleted = await this.noticiasRepository.delete({ id });
    if (noticiaDeleted.affected === 0) {
      throw new HttpException('Noticia no encontrada', HttpStatus.NOT_FOUND);
    }
    return noticiaDeleted;
  }
}
