import { IsDate, IsOptional, IsString } from 'class-validator';

export class CreateNoticiaDTO {
  @IsString()
  titulo: string;

  @IsDate()
  fechaPubli: Date;

  @IsString()
  lugar: string;

  @IsString()
  autor: string;

  @IsString()
  contenido: string;
}
export class UpdateNoticiaDTO {
  @IsString()
  @IsOptional()
  titulo?: string;

  @IsDate()
  @IsOptional()
  fechaPubli: Date;

  @IsString()
  @IsOptional()
  lugar?: string;

  @IsString()
  @IsOptional()
  autor?: string;

  @IsString()
  @IsOptional()
  contenido?: string;
}
